import Customer from "../../../models/Customer";
import connectDB from "../../../utils/connectDB";

export default async function handler(req,res) {
    try {
        await connectDB()
    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: "Faild",
            message : "error in connecting Db"
        })
        return;
    }
    
    if(req.method === "POST") {
        const data = re.body.data
        if(!data.name || !data.lastName || !data.email) {
            return res.status(400).json({
                status:"Faild" ,
                message : 'Invalid data'
            })
        }

        try{
            const customer =await Customer.create(data)
            res.status(201).json({
                status: "success" ,
                data : customer
            })
        } catch(err) {
            console.log(err)
            res.status(500).json({
                status: "Faild" ,
                message: "error in storing data"
            })
        }
    }


}