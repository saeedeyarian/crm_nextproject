import {Schema, model, models} from "mongoose";

const customerSchema = new Schema({
    name : {
        type : String,
        required : true,
        minLength : 1
    } ,
    lastName : {
        type : String,
        required : true,
        minLength : 1
    },
    email : {
        type : String,
        required : true,
        minLength : 1
    },
    phone : String ,
    address : String,
    postalCode : Number,
    date : Date,
    createAt :{
        type : Date , 
        default :() => Date.now()
    } ,
    updatedAt : {
        type : Date , 
        default :() => Date.now()
    },
    products : {
        type : Array ,
        default : []
    }
})

const Customer = models.Customer || model("Customer" , customerSchema)

export default Customer;