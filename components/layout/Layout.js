import Link from "next/link"

function Layout({children}) {
  return (
    <>
      <header className="header">
        <h2>CRM Project</h2>
        <Link href="/add-customer">Add customer</Link>
      </header>
      <div>
        {children}
      </div>
      <footer>
        Nextjs | CRM project
      </footer>
    </>
  )
}

export default Layout
