import React, { useState } from 'react'
import Form from '../modules/Form'
import {useRouter} from "next/router"

function AddCustomerPage() {

    const router = useRouter()

    const [form , setForm] = useState({
        name : "",
        lastName : "" ,
        email : "" ,
        phone : "" ,
        address : "" ,
        postalCode : "",
        date : "",
        products : []
    })

    const saveHandler = async() =>{
        const res = await fetch("/api/customer" , {
            method : "POST" ,
            body : JSON.stringify({data : form}) ,
            headers : {"Content-type" : "application/json"}
        })
        const data = await res.json()
        console.log(data)
        if(data.status === "success") {router.push("/")}
    }

    const cancelHandler = () => {
        setForm ({
            name : "",
            lastName : "" ,
            email : "" ,
            phone : "" ,
            address : "" ,
            postalCode : "",
            date : "",
            products : []
        })
    }


  return (
    <div>
      <h4>Add New Customer</h4>
      <Form form={form} setForm={setForm} />
      <div className="customer-page__buttons">
        <button className='cancel' onChange={cancelHandler}>Cancel</button>
        <button className='save' onChange={saveHandler}>Save</button>
      </div>
    </div>
  )
}

export default AddCustomerPage
