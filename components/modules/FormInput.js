
function FormInput({name,label,onChange,value,type}) {
  return (
    <div className="form-input">
      <label>{label}</label>
      <input type={type} name={name} id={name} value={value} onChange={onChange}/>
    </div>
  )
}

export default FormInput
